---
title: "Change of Basis using the Summation Convention"
---

Let us derive the change-of-basis matrix from bases \(\alpha\) to \(\beta\), given an arbitrary vector \(v\).
Let \(\alpha_i\) be the \( i \)th vector of basis \(\alpha\), and \(v^\alpha_i\) the \(i\)th coordinate of \(v\) with respect to \(\alpha\), similarly for \(\beta\).

We have 
\[ v = v^\alpha _ i \alpha_i = v^\beta _ i \beta _ i \label{eqn_1} \tag{1} \]
The representation of and \(\alpha\) vector with respect to \(\beta\) is given by scalars \(m _ {ij} \) with
\[ \alpha_i = m _ {ij} \beta_j \label{eqn_2} \tag{2} \]

Then
\[ v^\alpha_i \alpha_i = v^\alpha_i (m _ {ij} \beta_j) = (v^\alpha_i m _ {ij}) \beta_j = (v^\alpha_j m _ {ji}) \beta_i \label{eqn_3} \tag{3} \]

So \(v^\beta_i\) in terms of \(v^\alpha_i\) is
\[v^\beta _ i = v^\alpha_i m _ {ij} = m _ {ij} v^\alpha_i \label{eqn_4} \tag{4} \]

This is almost matrix-vector multiplication, which is given by e.g. \( b_i = m _ {ij} a _ j\), except for a swap of indices. This means that \(m _ {ij}\) find their places in the transpose of the change-of-basis matrix. Knowing this and looking at rows and columms, we see that the \(m _ {i * }\) scalars belonging to the representation of a single \(\alpha_i\) vector find their places in the column \(i\) of the change-of-basis matrix. This is in accordance to the definition given in Jim Hefferon's *Linear Algebra*, which is there given without a derivation.

In short
\[v^\beta _ i = \lbrack{M^T}\rbrack _ {ij} v^\alpha_j \label{eqn_5} \tag{5} \]
