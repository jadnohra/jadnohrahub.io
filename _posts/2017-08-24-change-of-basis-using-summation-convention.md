---
title: "Change of Basis using Summation Convention"
date: 2018-08-24
---

 * Let us derive the change-of-basis matrix from bases \(\alpha\) to \(\beta\)
 * Let \(\alpha_i\) be the \( i \)th vector of basis \\(\alpha\\), similarly for \(\beta\)
 * Let \(v\) be any vector, \(v_i\) and \(v'_i\) its \(i\)th coordinate with respect to \(\alpha\) and \(\beta\) respectively 
 * We have: \\( v = v_i \alpha_i = v'_i \beta_i \\)
 * Let the basis vectors of \( \alpha \) have the coordinates \(m _ {ij}\) with respect to \( \beta \): \\( \alpha_i = m _ {ij} \beta_j \\)
 * Then we get: \\( v_i \alpha_i = v_i (m _ {ij} \beta_j) = (v _ i m _ {ij}) \beta_j = (v_j m _ {ji}) \beta_i \\)
 * Hence, \\( v' _ i =  m _ {ji} v _ j \\)
 * But matrix-vector multiplication with the summation convection with matrix \( M=[m _ {ij}] \) looks like this: \( m _ {ij} v _ j \) (notice the indices). To express the above \( {v'} _ i\) \) as a matrix multiplication we use the transpose matrix \( M^T=[{m^T} _ {ij}]=[{m} _ {ji}] \) and can write:  \\( v' _ i =  {m^T} _ {ij} v_j \\) and we are done.
* We notice that the \( \beta \)-relative coordinates of each \( \alpha _ i \) form a column ( and not a row ) in matrix \( M \), in accordance with the definition (e.g in Jim Hefferon's *Linear Algebra*).
 


#### Motivation
As a first introduction to Linear Algebra, Jim Hefferon's freely [available](http://joshua.smcvt.edu/linearalgebra/) *Linear Algebra* is a great textbook. It is one of the few textbooks that we managed to finish with full exercises [back in the day](https://sites.google.com/site/77neuronsprojectperelman/retired/weeks/3-linear-algebra-by-hefferon-weeks-32-present-abstractions-better-brains-better-attitude-and-methods-less-baby-sitting). The result was quite a long list of errata that [got into the book](https://gitlab.com/jim.hefferon/linear-algebra/blob/c33cf6c580dc6225d4e85a606bd81b1c972c9f90/Acknowledgements).

The book's definition of the *change of basis* matrix stuck with me since then, bothering me for the fact that it was given as a definition. Last week, while practicing subtelties of the summation convention, I had an idea of how to derive it nicely.
